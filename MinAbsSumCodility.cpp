#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
int solution(int A[],int N){
	
	int sum=0,S2,max=0;
	for (int i=0;i<N;i++){ 
		A[i]=abs(A[i]);
		sum += A[i]; 
		S2 = sum / 2; 
		max = max < A[i] ? A[i] : max;
	}
	     
	int countSize = sizeof(int) * (max + 1); 
    int* count = (int*)malloc(countSize); 
    memset(count, 0, countSize); 
		
	for (int i = 0; i < N; i++){ 
        count[A[i]]++;             
    }
    
    int dpSize = sizeof(int) * (sum + 1);
    int* dp = (int*)malloc(dpSize);
    memset(dp, -1, dpSize);
    dp[0] = 0;
       
    
    for (int a = 1; a <= max; a++){
        if (count[a] <= 0){ 
            continue;    
        }
        for (int j = 0; j <= sum; j++){  
            if (dp[j] >= 0){  
                dp[j] = count[a]; 
            }
            else if (j >= a && dp[j - a] > 0){  
                dp[j] = dp[j - a] - 1;    
            }
        }
    }
    int result = sum;
    for (int p = 0; p <= sum/2; p++){
        if (dp[p] >= 0){
            int q = sum - p;
            int diff = q - p;
            result = diff < result ? diff : result; 
        }
    }
    return result;
}
