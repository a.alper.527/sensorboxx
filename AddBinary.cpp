#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
char* addBinary(char* a,char* b){ 
	int i = strlen(a)-1; 
	int j = strlen(b)-1;
	int carry=0,sizeC;	
    if(i>j){
        sizeC=i+1; //hangi dizi daha uzunsa c(toplam) dizisini onun uzunluguna esitledik
	}
	else sizeC=j+1;
	char* c=(char*)malloc((sizeC+2)*sizeof(char)); // c ye verdigimiz boyutun 2 fazlasi kadar heapte yer ayirdik. 
	//(2 fazlasi cunku 1 i toplamda artabilecek elde icin digeri de son indise otomatik konulan \0 icin.
	c[sizeC+1]='\0'; //dizinin son indisini 0 la bitiriyoruz
	while(i>=0||j>=0||carry){ 	//2 diziyi kapsayan ve carry i iceren while loop actik
		if(i>=0){ 
			carry += a[i]-'0';	//a dizisinin son indisini al, int e cevir, carry e ata. i yi 1 azalt.
			i--; 
		}
        else carry+= 0; //a dizisi bitmisse elde yoktur dolayisiyla carry degismesin diye 0 ekliyoruz.
		if(j>=0){ 
			carry += b[j]-'0'; //b dizisinin son indisini al, int e cevir, carry e ata. i yi 1 azalt.
			j--; 
		}
        else carry+= 0; //b dizisi bitmisse carry yoktur dolayisiyla carry degismesin diye 0 ekliyoruz.
	    char value = (carry%2)+'0'; /*a ve b dizisinin alt alta degerlerini toplayip carry e att�ktan sonra bakiyoruz. carry 0 sa 0+0; 1 se 1+0;
		2 ise 1+1 toplanmis demektir. Bunun modunu aldigimizda binary de toplam sonuclarinin aynisini aliriz. toplami value ya karaktere donusturerek
		atiyoruz. */
        if(sizeC>=0) c[sizeC--]=value; /* sizeC 0 olana kadar value'yu diziye sondan basa dogru atiyoruz.*/
        if(i<0 && j<0 && carry>1)  // diziler biter ve carry 1 den buyukse (yani 2 ise) 1+1 i son adimda topladik elde basa eklenecek 1 kaldi demektir. Basa 1 ekliyoruz.    
        {
        c[0] = '1';
        return c;  
        }
	    carry/=2;  /*c dizisine toplam degeri value yu attiktan sonra eldeyi 2 ye boluyoruz. bu bolum sonuclari da binary deki elde sonuclariyla ayni cikiyor. Carry i yenisiyle guncelliyoruz.*/      
	}  
    return c+1; //carry yoksa basta 0 kalacak bastaki 0 i almiyoruz.
}
int main(){
	char arrA[]="1010";
	char arrB[]="1011";	
	char* c= addBinary(arrA,arrB);
	printf("%s",c);
	
}




