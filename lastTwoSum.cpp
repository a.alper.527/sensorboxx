#include <stdio.h>
#include <stdlib.h>

#define SIZE(n)   n*sizeof(int) / sizeof(int)

int* twoSum(int* nums, int numsSize, int target, int* returnSize){

    
for (int i = 0; i < numsSize; i++)
    {
        for (int j = i+1; j < numsSize; j++)
        {
            if (nums[i] + nums[j] == target)
            {
                *returnSize = 2;
                int *rtn = (int *)malloc(2*sizeof(int));
                rtn[0] = i;
                rtn[1] = j;
                return rtn;
            }
        }
    }
    return NULL;
}


int main()
{
    int n,val,target;
    printf("ENTER ARRAY SIZE:");
    scanf("%d",&n);
    
    int* arr =(int*) malloc(n*sizeof(int));
    int* returnSize = (int*)malloc(2*sizeof(int)); 
    
    printf("ENTER ARRAY ELEMENTS:");
    for(int i=0;i<n;i++){
        
        scanf("%d",&val);
        *(arr+i) = val ;
    }
    printf("ENTER TARGET :");
    scanf("%d",&target);
    
    int* rtn = twoSum(arr,SIZE(n),target,returnSize);  
    
    if(rtn != NULL)
    printf("\n[%d,%d]",*rtn,*(rtn+1));
    else
    printf("\n NONE OF THE SUMS ADD UP TO TE TARGET!");
    
   
    free(arr);    
    return 0;
}

